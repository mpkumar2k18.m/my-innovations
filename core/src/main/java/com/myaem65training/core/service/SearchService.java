package com.myaem65training.core.service;

import java.util.Map;

import com.google.gson.JsonArray;

public interface SearchService {
	
	public Map<String, String> createPageSearchPredicateMap(String queryString,  String queryPagesLocation);
	
	public Map<String, String> createPageSearchPredicateMap(String queryString);
	
	
}
