package com.myaem65training.core.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.myaem65training.core.service.SearchService;

public class SearchServiceImpl implements SearchService {
	

	public Map<String, String> createPageSearchPredicateMap(String queryString, String queryPagesLocation) {
		// TODO Auto-generated method stub
		
		Map<String, String> pagePredicate = new HashMap<>();

		

		/**
		 * Configuring the Map for the predicate
		 */
		pagePredicate.put("path", queryPagesLocation);
		pagePredicate.put("type", "cq:page");
		pagePredicate.put("group.p.or", "true");
		pagePredicate.put("group.1_fulltext", queryString);
		pagePredicate.put("group.1_fulltext.relPath", "jcr:content");
		pagePredicate.put("p.offset", "0"); // same as query.setStart(0) below
		pagePredicate.put("p.limit", "20"); // same as query.setHitsPerPage(20)
		
		return pagePredicate;
	}
	
	
	public Map<String, String> createPageSearchPredicateMap(String queryString) {
		// TODO Auto-generated method stub
		
		Map<String, String> pagePredicate = new HashMap<>();

		

		/**
		 * Configuring the Map for the predicate
		 */
		pagePredicate.put("path", "/content/we-retail");
		pagePredicate.put("type", "cq:page");
		pagePredicate.put("group.p.or", "true");
		pagePredicate.put("group.1_fulltext", queryString);
		pagePredicate.put("group.1_fulltext.relPath", "jcr:content");
		
		return pagePredicate;
	}

}
