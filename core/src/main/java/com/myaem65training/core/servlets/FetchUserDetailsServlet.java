package com.myaem65training.core.servlets;

import java.io.IOException;
import java.util.Iterator;

import javax.jcr.AccessDeniedException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=Fetch User Details Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/fetchUserDetails",
		"sling.servlet.extensions=" + "json" })
public class FetchUserDetailsServlet extends SlingSafeMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2057483906330608157L;
	private static final Logger log = LoggerFactory.getLogger(FetchUserDetailsServlet.class);
	@Reference
	private QueryBuilder builder;
	private Session session;

	@SuppressWarnings({ "null", "unused" })
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
        
		try {

			ResourceResolver resourceResolver = request.getResourceResolver();
			Resource resource = resourceResolver.getResource("/home");
			session = (resourceResolver != null ? resourceResolver.adaptTo(Session.class) : null);
			UserManager userManager = ((JackrabbitSession) session).getUserManager();
			
			String param = request.getParameter("param");
			log.info("Search term is: {}", param);
	        
			
			Iterator<Authorizable> userIterator = userManager.findAuthorizables("jcr:primaryType", "rep:User");
			Iterator<Authorizable> groupIterator = userManager.findAuthorizables("jcr:primaryType", "rep:Group");

			

			while (userIterator.hasNext()) {

				log.info("Getting user");

				Authorizable user = userIterator.next();

				if (!user.isGroup() && user.getID().contains(param)) {
					log.info("User found: {}", user.getID());
					System.out.println("User found: {}"+ user.getID());
					
				
				}
			}

			while (groupIterator.hasNext()) {

				log.info("Getting group");

				Authorizable group = groupIterator.next();

				if (group.isGroup() && param.equalsIgnoreCase(group.getID())) {
					log.info("Group found {}", group.getID());
					
				}
			}
			
		} catch (AccessDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedRepositoryOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			if(session != null) {
				
				session.logout();
			}
		}
		
		
	}

}
